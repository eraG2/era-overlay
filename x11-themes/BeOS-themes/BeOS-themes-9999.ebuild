# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="GTK Themes reminiscient of BeOS"
HOMEPAGE="http://none.none"
SRC_URI="http://pkg.e9d.org/distfiles/BeOS-themes-9999.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ia64 ppc ppc64 s390 sh sparc x86"

src_install() {
	dodir /usr/share/themes
	dodir /usr/share/icons
	cp -R "${S}/themes/BeOS-R5" "${D}/usr/share/themes" || die "Install failed!"
	cp -R "${S}/themes/BeOS-R5-XFWM" "${D}/usr/share/themes" || die "Install failed!"
	cp -R "${S}/themes/BeOS-Again" "${D}/usr/share/themes" || die "Install failed!"
	cp -R "${S}/icons/beos-icons" "${D}/usr/share/icons" || die "Install failed!"
	cp -R "${S}/icons/beos-r5" "${D}/usr/share/icons" || die "Install failed!"
}
