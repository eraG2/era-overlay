# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="ERA G^2/Linux baselayout build system + initial configuration files"
HOMEPAGE="https://gitlab.com/eraG2"
SRC_URI="http://pkg.e9d.org/distfiles/${P}.tar.gz"

LICENSE="AGPL-3+"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

pkg_info() {
	ewarn "To finish the install of ERA G^2/Linux, AS ROOT, run 'sh /stage1 && sh /stage2' from the root directory"
	echo "This process will install configuration files and re-merge the system"
	echo "For end-users wishing to turn Gentoo into ERA, this is all that needs to be done"
	echo "For developers, you should then build a sqaushfs image of the rootfs for mass distribution"
}
