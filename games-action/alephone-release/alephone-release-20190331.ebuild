# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Alpehone is the open-source continuation of Bungie's Marathon 2 Game Engine"
HOMEPAGE="https://alephone.lhowon.org"
SRC_URI="https://github.com/Aleph-One-Marathon/alephone/archive/release-20190331.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"

DEPEND="media-libs/sdl2-ttf
	media-libs/sdl2-net"

src_configure() {
	./autogen.sh
	econf
}
